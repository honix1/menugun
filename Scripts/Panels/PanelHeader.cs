﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelHeader : Panel {

        public Button dropDownButton;
        public Image arrow;
       
        private bool active = true;

        private List<ValuePanel> childs = new List<ValuePanel>();

        void Start() {
            dropDownButton.onClick.AddListener(OnDropDownClick);    
        }

        void OnDropDownClick() {
            active = !active;
            arrow.transform.rotation = active ? Quaternion.Euler(0, 0, 0) : Quaternion.Euler(0, 0, 90);
            childs.ForEach(x => x.gameObject.SetActive(active));
        }

        public override void SetName(string name) {
            GetComponentInChildren<Text>().text = name;
        }

        public PanelHeader Make(string s) {
            GameObject go = Instantiate(gameObject, transform.parent);
            PanelHeader hp = go.GetComponent<PanelHeader>();

            hp.Identifier = s;

            return hp;
        }

        public void Add(ValuePanel p) {
            childs.Add(p);
        }
    }
}
