﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelVector2 : ValuePanel {

        public InputField inputFieldX;
        public Slider sliderX;
        public InputField inputFieldY;
        public Slider sliderY;

        private Vector2 value;

        void Start() {
            // for some lucky reason this lines dont recurse each other
            inputFieldX.onEndEdit.AddListener((s) => {
                value.x = float.Parse(s);
                Refresh();
            });
            sliderX.onValueChanged.AddListener((f) => {
                value.x = f;
                Refresh();
            });

            inputFieldY.onEndEdit.AddListener((s) => {
                value.y = float.Parse(s);
                Refresh();
            });
            sliderY.onValueChanged.AddListener((f) => {
                value.y = f;
                Refresh();
            });
        }

        void Refresh() {
            SetValue(value);
        }

        void SetRange(float min, float max) {
            sliderX.minValue = min;
            sliderX.maxValue = max;
            sliderY.minValue = min;
            sliderY.maxValue = max;
        }

        void SetValue(Vector2 value) {
            this.value = value;

            sliderX.value = value.x;
            sliderY.value = value.y;

            SetRange(
                Math.Min(Math.Min(Math.Min(sliderX.minValue, sliderY.minValue), value.x), value.y),
                Math.Max(Math.Max(Math.Max(sliderX.maxValue, sliderY.maxValue), value.x), value.y)
            );

            if (!inputFieldX.isFocused) {
                inputFieldX.text = value.x.ToString();
            }
            if (!inputFieldY.isFocused) {
                inputFieldY.text = value.y.ToString();
            }
        }

        Vector2 GetValue() {
            return value;
        }

        public override bool CanHandle(Type t) {
            return t == typeof(Vector2);
        }

        public override void Subscribe(Action action) {
            inputFieldX.onEndEdit.AddListener(x => action.Invoke());
            sliderX.onValueChanged.AddListener(x => action.Invoke());

            inputFieldY.onEndEdit.AddListener(y => action.Invoke());
            sliderY.onValueChanged.AddListener(y => action.Invoke());
        }

        public override void AfterLink() {
            MenuRangeAttribute ra = linker.customAttributes.FirstOrDefault(x => x is MenuRangeAttribute) as MenuRangeAttribute;
            if (ra != null) {
                SetRange(ra.min, ra.max);
            }

            Subscribe(() => linker.SetValue(GetValue()));
        }

        public override void Fetch() {
            SetValue((Vector2) linker.GetValue());
        }
    }
}
