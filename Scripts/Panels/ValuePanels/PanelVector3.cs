﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelVector3 : ValuePanel {

        public InputField inputFieldX;
        public Slider sliderX;
        public InputField inputFieldY;
        public Slider sliderY;
        public InputField inputFieldZ;
        public Slider sliderZ;

        private Vector3 value;

        void Start() {
            // for some lucky reason this lines dont recurse each other
            inputFieldX.onEndEdit.AddListener((s) => {
                value.x = float.Parse(s);
                Refresh();
            });
            sliderX.onValueChanged.AddListener((f) => {
                value.x = f;
                Refresh();
            });

            inputFieldY.onEndEdit.AddListener((s) => {
                value.y = float.Parse(s);
                Refresh();
            });
            sliderY.onValueChanged.AddListener((f) => {
                value.y = f;
                Refresh();
            });

            inputFieldZ.onEndEdit.AddListener((s) => {
                value.z = float.Parse(s);
                Refresh();
            });
            sliderZ.onValueChanged.AddListener((f) => {
                value.z = f;
                Refresh();
            });
        }

        void Refresh() {
            SetValue(value);
        }

        void SetRange(float min, float max) {
            sliderX.minValue = min;
            sliderX.maxValue = max;
            sliderY.minValue = min;
            sliderY.maxValue = max;
            sliderZ.minValue = min;
            sliderZ.maxValue = max;
        }

        void SetValue(Vector3 value) {
            this.value = value;

            sliderX.value = value.x;
            sliderY.value = value.y;
            sliderZ.value = value.z;

            SetRange(
                Math.Min(Math.Min(Math.Min(Math.Min(Math.Min(sliderX.minValue, sliderX.minValue), sliderZ.minValue), value.x), value.y), value.z),
                Math.Max(Math.Max(Math.Max(Math.Max(Math.Max(sliderX.maxValue, sliderX.maxValue), sliderZ.maxValue), value.x), value.y), value.z)
            );

            if (!inputFieldX.isFocused) {
                inputFieldX.text = value.x.ToString();
            }
            if (!inputFieldY.isFocused) {
                inputFieldY.text = value.y.ToString();
            }
            if (!inputFieldZ.isFocused) {
                inputFieldZ.text = value.z.ToString();
            }
        }

        Vector3 GetValue() {
            return value;
        }

        public override bool CanHandle(Type t) {
            return t == typeof(Vector3);
        }

        public override void Subscribe(Action action) {
            inputFieldX.onEndEdit.AddListener(x => action.Invoke());
            sliderX.onValueChanged.AddListener(x => action.Invoke());

            inputFieldY.onEndEdit.AddListener(y => action.Invoke());
            sliderY.onValueChanged.AddListener(y => action.Invoke());

            inputFieldZ.onEndEdit.AddListener(z => action.Invoke());
            sliderZ.onValueChanged.AddListener(z => action.Invoke());
        }

        public override void AfterLink() {
            MenuRangeAttribute ra = linker.customAttributes.FirstOrDefault(x => x is MenuRangeAttribute) as MenuRangeAttribute;
            if (ra != null) {
                SetRange(ra.min, ra.max);
            }

            Subscribe(() => linker.SetValue(GetValue()));
        }

        public override void Fetch() {
            SetValue((Vector3)linker.GetValue());
        }
    }
}
