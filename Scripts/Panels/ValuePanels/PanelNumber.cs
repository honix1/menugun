﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace MenuGun {
    public class PanelNumber : ValuePanel {

        public InputField inputField;
        public Slider slider;

        float value;

        void Start() {
            // for some lucky reason this lines dont recurse each other
            inputField.onEndEdit.AddListener((s) => {
                value = float.Parse(s);
                Refresh();
            });
            slider.onValueChanged.AddListener((f) => {
                value = f;
                Refresh();
            });
        }

        void Refresh() {
            SetValue(value);
        }

        void SetRange(float min, float max) {
            slider.minValue = min;
            slider.maxValue = max;
        }

        void SetValue(float value) {
            this.value = value;

            SetRange(
                Math.Min(value, slider.minValue), 
                Math.Max(value, slider.maxValue)
            );

            slider.value = value;

            if (!inputField.isFocused) {
                inputField.text = value.ToString ();
            }
        }

        float GetValue() {
            return slider.value;
        }

        void SetDiscrette(bool b) {
            if (b) {
                inputField.characterValidation = InputField.CharacterValidation.Integer;
            } else {
                inputField.characterValidation = InputField.CharacterValidation.Decimal;
            }
            slider.wholeNumbers = b;
        }

        bool IsInt(Type t) {
            return t == typeof(int) || t == typeof(Int16) || t == typeof(Int32) || t == typeof(Int64);
        }

        public override bool CanHandle(Type t) {
            return IsInt(t) || t == typeof(float) || t == typeof(Single) || t == typeof(double);
        }

        public override void Subscribe(Action action) {
            inputField.onEndEdit.AddListener((x) => action.Invoke());
            slider.onValueChanged.AddListener((x) => action.Invoke());
        }

        public override void AfterLink() {
            MenuRangeAttribute ra = linker.customAttributes.FirstOrDefault(x => x is MenuRangeAttribute) as MenuRangeAttribute;
            if (ra != null) {
                SetRange(ra.min, ra.max);
            }

            Type type = linker.type;

            if (IsInt(type)) {
                Subscribe(() => linker.SetValue((int)GetValue()));
            } else if (type == typeof(float)) {
                Subscribe(() => linker.SetValue((float)GetValue()));
            } else if (type == typeof(Single)) {
                Subscribe(() => linker.SetValue((Single)GetValue()));
            } else if (type == typeof(double)) {
                Subscribe(() => linker.SetValue((double)GetValue()));
            } else {
                throw new Exception("Can't resolve type of number");
            }

            SetDiscrette(IsInt(type));
        }

        public override void Fetch() {
            Type type = linker.type;

            if (IsInt(type)) {
                SetValue((int)linker.GetValue());
            } else {
                SetValue((float)linker.GetValue());
            }
        }
    }
}
