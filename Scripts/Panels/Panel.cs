﻿using UnityEngine;

namespace MenuGun {
    public abstract class Panel : MonoBehaviour {

        private string identifier;
        public string Identifier {
            get {
                return identifier;
            }
            set {
                identifier = value;
                SetName(identifier);
            }
        }

        abstract public void SetName(string name);

    }
}
